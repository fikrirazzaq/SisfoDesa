package com.example.tel_c.sisfodesa.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TEL-C on 4/7/17.
 */

public class StratMisi extends RealmObject {

    @PrimaryKey
    private int id_strat;
    private String isi;
    private Misi misi;

    public Misi getMisi() {
        return misi;
    }

    public void setMisi(Misi misi) {
        this.misi = misi;
    }

    public int getId_strat() {
        return id_strat;
    }

    public void setId_strat(int id_strat) {
        this.id_strat = id_strat;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}
