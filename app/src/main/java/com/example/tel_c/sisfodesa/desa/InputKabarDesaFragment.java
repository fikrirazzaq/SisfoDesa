package com.example.tel_c.sisfodesa.desa;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tel_c.sisfodesa.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InputKabarDesaFragment extends Fragment {


    public InputKabarDesaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input_kabar_desa, container, false);
    }

}
