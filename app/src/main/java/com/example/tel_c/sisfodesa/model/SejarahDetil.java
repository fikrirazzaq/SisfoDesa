package com.example.tel_c.sisfodesa.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TEL-C on 4/7/17.
 */

public class SejarahDetil extends RealmObject {

    @PrimaryKey
    private int id_sejarah_detil;
    private String isi;
    private SejarahDesa sejarahDesa;

    public SejarahDesa getSejarahDesa() {
        return sejarahDesa;
    }

    public void setSejarahDesa(SejarahDesa sejarahDesa) {
        this.sejarahDesa = sejarahDesa;
    }

    public int getId_sejarah_detil() {
        return id_sejarah_detil;
    }

    public void setId_sejarah_detil(int id_sejarah_detil) {
        this.id_sejarah_detil = id_sejarah_detil;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}
