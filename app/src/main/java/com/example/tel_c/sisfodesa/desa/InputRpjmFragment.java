package com.example.tel_c.sisfodesa.desa;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tel_c.sisfodesa.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InputRpjmFragment extends Fragment {


    public InputRpjmFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input_rpjm, container, false);
    }

}
