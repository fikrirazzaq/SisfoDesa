package com.example.tel_c.sisfodesa.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TEL-C on 4/7/17.
 */

public class SejarahDesa extends RealmObject {

    @PrimaryKey
    private int id_sejarah;
    private String nama;

    public int getId_sejarah() {
        return id_sejarah;
    }

    public void setId_sejarah(int id_sejarah) {
        this.id_sejarah = id_sejarah;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
