package com.example.tel_c.sisfodesa.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TEL-C on 4/7/17.
 */

public class Misi extends RealmObject {

    @PrimaryKey
    private int id_misi;
    private String misi;
    private Visi visi;

    public Visi getVisi() {
        return visi;
    }

    public void setVisi(Visi visi) {
        this.visi = visi;
    }

    public int getId_misi() {
        return id_misi;
    }

    public void setId_misi(int id_misi) {
        this.id_misi = id_misi;
    }

    public String getMisi() {
        return misi;
    }

    public void setMisi(String misi) {
        this.misi = misi;
    }
}
