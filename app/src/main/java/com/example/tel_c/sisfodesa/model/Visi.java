package com.example.tel_c.sisfodesa.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TEL-C on 4/7/17.
 */

public class Visi extends RealmObject {

    @PrimaryKey
    private int id_visi;
    private String visi;

    public int getId_visi() {
        return id_visi;
    }

    public void setId_visi(int id_visi) {
        this.id_visi = id_visi;
    }

    public String getVisi() {
        return visi;
    }

    public void setVisi(String visi) {
        this.visi = visi;
    }
}
