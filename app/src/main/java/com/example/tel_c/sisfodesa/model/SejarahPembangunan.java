package com.example.tel_c.sisfodesa.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TEL-C on 4/7/17.
 */

public class SejarahPembangunan extends RealmObject {

    @PrimaryKey
    private int id_sejarah_pemb;
    private String tahun;
    private String kejadian;
    private SejarahDetil sejarahDetil;

    public SejarahDetil getSejarahDetil() {
        return sejarahDetil;
    }

    public void setSejarahDetil(SejarahDetil sejarahDetil) {
        this.sejarahDetil = sejarahDetil;
    }

    public int getId_sejarah_pemb() {
        return id_sejarah_pemb;
    }

    public void setId_sejarah_pemb(int id_sejarah_pemb) {
        this.id_sejarah_pemb = id_sejarah_pemb;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getKejadian() {
        return kejadian;
    }

    public void setKejadian(String kejadian) {
        this.kejadian = kejadian;
    }
}